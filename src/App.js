import Animal from "./component/Animal";

function App() {
  return (
    <div>
      <Animal kind="dog"/>
    </div>
  );
}

export default App;
