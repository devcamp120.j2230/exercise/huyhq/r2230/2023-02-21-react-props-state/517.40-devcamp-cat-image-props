import { Component } from "react";
import imgCat from "../asset/images/cat.jpg";
class Animal extends Component{
    render(){
        return(
            <div>
                {this.props.kind == "cat" ? <img src={imgCat}/> : <p>meow not found :)</p>} 
            </div>
        );
    }
}

export default Animal;